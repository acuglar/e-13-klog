from flask import Flask

def init_app(app: Flask):
    from .klog_views import bp
    app.register_blueprint(bp)
