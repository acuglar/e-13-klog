from sqlalchemy.sql.sqltypes import Date
from sqlalchemy import Column, Integer, String
from app import db


class KlogModel(db.Model):
    __tablename__ = "posts"

    id = Column(Integer, primary_key=True)

    title = Column(String(50), nullable=False)
    date = Column(String(50), nullable=False)
    content = Column(String(), nullable=False)
    author = Column(String(50), nullable=False)
    email = Column(String(50), nullable=False)
    
    def __str__(self):
        return f"<{self.title} - {self.id}>"

    def __repr__(self):
        return f"<{self.title} - {self.id}>"